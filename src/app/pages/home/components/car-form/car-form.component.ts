import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Car } from "../../../../shared";

@Component({
  selector: "app-car-form",
  templateUrl: "./car-form.component.html",
  styleUrls: ["./car-form.component.css"],
})
export class CarFormComponent implements OnInit {
  carForm: FormGroup;
  URL_REGEXP = "(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?";

  constructor(
    public dialogRef: MatDialogRef<CarFormComponent>,
    @Inject(MAT_DIALOG_DATA) public car: Car
  ) {}

  ngOnInit(): void {
    this.carForm = new FormGroup({
      manufacturer: new FormControl(this.car.manufacturer, Validators.required),
      type: new FormControl(this.car.type, Validators.required),
      engineDisplacement: new FormControl(
        this.car.engineDisplacement,
        Validators.required
      ),
      color: new FormControl(this.car.color),
      design: new FormControl(this.car.design),
      dateOfManufacture: new FormControl(
        this.car.dateOfManufacture,
        Validators.required
      ),
      websiteOfManufacture: new FormControl(
        this.car.websiteOfManufacture,
        Validators.pattern(this.URL_REGEXP)
      ),
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
