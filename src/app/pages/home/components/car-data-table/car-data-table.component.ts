import {
  Component,
  Input,
  OnChanges,
  OnInit,
  ViewChild,
  SimpleChanges,
  Output,
  EventEmitter,
} from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { MatPaginator } from "@angular/material/paginator";
import { MatDialog } from "@angular/material/dialog";
import { Car, DefaultCar } from "../../../../shared/";
import { CarFormComponent } from "./../car-form/car-form.component";

@Component({
  selector: "app-car-data-table",
  templateUrl: "./car-data-table.component.html",
  styleUrls: ["./car-data-table.component.css"],
})
export class CarDataTableComponent implements OnInit, OnChanges {
  @Input() title: string;
  @Input() data: Array<Car> = [] as Array<Car>;
  @Output() newCar = new EventEmitter<string>();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: MatTableDataSource<Car>;
  displayedColumns: string[];

  constructor(public dialog: MatDialog, private _sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.displayedColumns = this.getColumns(new DefaultCar());
    this.sortAndPlaginate();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["data"]) {
      this.sortAndPlaginate();
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CarFormComponent, {
      width: "40vw",
      data: {} as Car,
    });

    dialogRef.afterClosed().subscribe((newCar) => {
      if (typeof newCar == "object") {
        newCar.id = this.data.length + 1;
        this.newCar.emit(newCar);
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.sortAndPlaginate();
  }

  sortAndPlaginate() {
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  splitCamelCase(inputStr: string) {
    return inputStr.replace(/([A-Z])/g, " $1").replace(/^./, function (str) {
      return str.toUpperCase();
    });
  }

  getColumns(data: object) {
    return Object.keys(data).map((key) => {
      return key;
    });
  }

  formatTableValue(column: string, input: any) {
    if (column === "dateOfManufacture") {
      return this.formatEpochToDateString(input);
    } else if (column === "color") {
      return this._sanitizer.bypassSecurityTrustHtml(
        `<div style="background-color: ${input}; padding: 10px; border: 1px solid black;"></div>`
      );
    }
    return input;
  }

  formatEpochToDateString(input: number) {
    const offset = new Date(input).getTimezoneOffset();
    return new Date(new Date(input).getTime() - offset * 60 * 1000)
      .toISOString()
      .split("T")[0];
  }
}
