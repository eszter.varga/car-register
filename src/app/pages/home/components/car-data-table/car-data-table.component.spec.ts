import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CarDataTableComponent } from './car-data-table.component';

describe('CarDataTableComponent', () => {
  let component: CarDataTableComponent;
  let fixture: ComponentFixture<CarDataTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CarDataTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
