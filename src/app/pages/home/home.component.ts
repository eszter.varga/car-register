import { Component, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { Car, CarService } from "../../shared";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
})
export class HomeComponent implements OnInit {
  public cars: Car[] = [];
  private subscriptions = new Subscription();

  constructor(public carService: CarService) {}

  ngOnInit() {
    this.subscriptions.add(
      this.carService.isUpdated.subscribe(() => {
        this.getCars();
      })
    );
  }

  getCars() {
    this.subscriptions.add(
      this.carService.load().subscribe((cars: Car[]) => {
        if (cars) {
          this.cars = cars;
        }
      })
    );
  }

  addCar(newCar: Car) {
    this.cars = [...this.cars, newCar];
    this.carService.setCars(this.cars);
  }

  public ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
