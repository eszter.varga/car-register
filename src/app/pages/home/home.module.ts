import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared";

import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./home.component";
import { CarDataTableComponent } from "./components/car-data-table/car-data-table.component";
import { CarFormComponent } from './components/car-form/car-form.component';

@NgModule({
  declarations: [HomeComponent, CarDataTableComponent, CarFormComponent],
  imports: [CommonModule, HomeRoutingModule, SharedModule],
})
export class HomeModule {}
