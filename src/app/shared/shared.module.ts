import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "./design/material/material.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { HeaderComponent } from "./components/layout/header/header.component";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
  ],
  exports: [
    MaterialModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    HeaderComponent,
  ],
})
export class SharedModule {}
