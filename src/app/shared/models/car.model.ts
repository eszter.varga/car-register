export interface Car {
  id: number;
  manufacturer: string;
  type: string;
  engineDisplacement: number;
  color?: string;
  design?: String;
  dateOfManufacture: number;
  websiteOfManufacture?: string;
}

export class DefaultCar implements Car {
  id: 0;
  manufacturer = "";
  type = "";
  engineDisplacement = 0;
  color = "#db0000";
  design = "";
  dateOfManufacture = 0;
  websiteOfManufacture = "";
}
