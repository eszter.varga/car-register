import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { of, BehaviorSubject } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { ToastService } from "./toast.service";
import { environment } from "../../../environments/environment";
import { Car } from "../models";

@Injectable({
  providedIn: "root",
})
export class CarService {
  url = `${environment.apiUrl}/cars`;

  cars: Car[];
  private _isDataUpdated = new BehaviorSubject<any>(null);

  constructor(public http: HttpClient, public toastService: ToastService) {}

  load() {
    if (this.cars) {
      return of(this.cars);
    } else {
      return this.http.get<Car[]>(`${this.url}`).pipe(
        map((result) => this.process(result)),
        catchError((e) => this.toastService.handleError(e))
      );
    }
  }

  process(cars: Car[]): Car[] {
    this.cars = cars;
    return this.cars;
  }

  setCars(cars: Car[]) {
    return this.http.post(`${this.url}`, { cars }).pipe(
      map(() => {
        this.update();
        this.toastService.showSuccess("New car added successfully!");
      }),
      catchError((e) => this.toastService.handleError(e))
    );
  }

  get isUpdated() {
    return this._isDataUpdated.asObservable();
  }

  update() {
    this._isDataUpdated.next(null);
  }
}
